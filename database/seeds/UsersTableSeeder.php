<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => "Ratheep",
                'last_name' => "Jaya",
                'email' => "ratheepan93@gmail.com",
                'password' => Hash::make('123456'),
                'role_id' => 1,
                'is_active' => "Yes"
            ]
        ];
        foreach($users as $user){
            App\User::create($user);
        }
    }
}
