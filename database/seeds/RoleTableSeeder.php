<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Admin',
                'permissions' => ''
            ],
            [
                'name' => 'Editor',
                'permissions' => ''
            ],
            [
                'name' => 'Modarator',
                'permissions' => ''
            ]
        ];
        foreach($roles as $role){
            Role::create($role);
        }
    }
}
