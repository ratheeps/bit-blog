<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{
    public function handler()
    {
        $user = auth()->user();
        return view('dashboard', compact('user'));
    }
}
