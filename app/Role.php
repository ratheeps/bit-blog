<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'permissions'];

    public function users()
    {
        $this->hasMany(User::class);
    }
}
