@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12 p-2">
            <div class="float-left">
                <h1>Posts</h1>
            </div>
            <div class="float-right">
                <a href="" class="btn btn-success">Create</a>
            </div>
        </div>
        <div class="col-12">
            <table class="table table-dark">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->category->name ?? "N/A" }}</td>
                            <td>{{ $post->created_at->toDateTimeString() }}</td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
