<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    @include("layouts.inc.top-nav")
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 d-none d-md-block bg-light sidebar">
                @include("layouts.inc.sidebar")
            </div>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 main-content">
                @yield('content')
            </main>
        </div>
    </div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
